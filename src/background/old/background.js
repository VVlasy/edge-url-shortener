import { makeRequest, makeRequestRaw } from './requests';
import { loadStoredData, openOauthWebFlow, openOauthWebFlowLegacy } from './oauth';

export function start() {

  window.loadedSettings = { // defaults
    settingsShowNotification: true,
    settingsShowCurrentUrlShorten: true,
    settingsAutoUrlShorten: false
  };

  window.accesstoken = null;
  window.defaultgroup = null;
  window.loggedin = false;

  // Continue with extension setup
  setup();

  // Hook context menu events
  browser.contextMenus.onClicked.addListener((info, tab) => {
    if (info.menuItemId == "url-shorten" || info.menuItemId == "current-url-shorten") {
      processShortenUserRequest(info.menuItemId == "url-shorten" ? info.linkUrl : tab.url);
    }

    if (info.menuItemId == "image-url-shorten") {
      console.log(Object.keys(info));
      console.log(Object.values(info));
      processShortenUserRequest(info.srcUrl);
    }

    if (info.menuItemId == "disable-auto-shorten") {
      window.loadedSettings.settingsAutoUrlShorten = false;
      browser.storage.local.set({
        settings: window.loadedSettings
      });

      handleSettingsChange(window.loadedSettings);
    }
  });

  // Hook messaging events
  browser.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request.action) {
      console.log("Received message on backend: " + request.action);

      switch (request.action) {
        case "oauthlogin":
          console.log("Starting oAuth 2 web flow");
          let isIE = /*@cc_on!@*/false || !!document.documentMode;
          let isEdge = !isIE && !!window.StyleMedia;
          if (!isEdge) {
            let uri = browser.identity.getRedirectURL("oauth2");
            let requiredUriEdge = 'https://daaakbkflpohmdmnojeedpppdlnnckgn.chromiumapp.org/oauth2';
            let requiredUriChrome = 'https://kphpbojgjgogohhkaenjmbeendnhpedp.chromiumapp.org/oauth2';
            
            if (uri == requiredUriEdge || uri == requiredUriChrome) // If key field is missing it will use legacy login to avoid bit.ly api errors
              openOauthWebFlow(() => { setup(); });
            else
              openOauthWebFlowLegacy(() => { setup(); });
          }
          else
            openOauthWebFlowLegacy(() => { setup(); });
          break;
        case "logout":
          logout();
          break;
        case "setup":
          setup();
          break;
        case "reload-settings":
          browser.storage.local.get("settings", (items) => {
            handleSettingsChange(items.settings);
          });
          break;
        default:
          console.error("Unknown background action");
      }
    }
  });
}


function setup() {
  console.log("Setting up extension...");
  loadStoredData(function (items) {
    handleSettingsChange(items.settings);

    console.log("Retrieved token from storage");
    console.debug(items.accesstoken);

    if (items.accesstoken && items.defaultgroup) {
      console.log("Logged in");
      window.loggedin = true;

      //browser.browserAction.setPopup({ popup: "/popup/popup.html" });
      browser.storage.local.set({ state: 'loggedIn' });
      browser.runtime.sendMessage({ stateChanged: true });

      window.accesstoken = items.accesstoken;
      window.defaultgroup = items.defaultgroup;

      // Change to active icon
      changeIconActiveState(true);

    } else {
      console.log("Not logged in");
      window.loggedin = false;

      //browser.browserAction.setPopup({ popup: "/popup/login.html" });
      browser.storage.local.set({ state: 'loggedOut' });
      browser.runtime.sendMessage({ stateChanged: true });

      // Change to inactive icon
      changeIconActiveState(false);
    }

  }, function (error) {
    console.log("Failed getting token from storage");
    console.error(error);
  });
}

function logout() {
  console.log("Logging out...");
  browser.storage.local.remove(["accesstoken", "groups", "window.defaultgroup"]);
  setup();
}

function createContextMenuItems() {
  // First clear all contextmenus
  browser.contextMenus.removeAll();

  console.log("Creating context menu items");
  browser.contextMenus.create({
    id: "url-shorten",
    title: browser.i18n.getMessage("shortenUrlContextItemTitle"),
    contexts: ['link']
  });

  browser.contextMenus.create({
    id: "image-url-shorten",
    title: browser.i18n.getMessage("shortenImageUrlContextItemTitle"),
    contexts: ['image']
  });

  if (window.loadedSettings.settingsShowCurrentUrlShorten) {
    browser.contextMenus.create({
      id: "current-url-shorten",
      title: browser.i18n.getMessage("shortenCurrentUrlContextItemTitle"),
      contexts: ['page']
    });
  }

  if (window.loadedSettings.settingsAutoUrlShorten) {
    browser.contextMenus.create({
      id: "disable-auto-shorten",
      title: browser.i18n.getMessage("disableAutoShortenContextItemTitle"),
      contexts: ['browser_action']
    });
  }
}

function changeIconActiveState(active) {
  console.log("Changing icon state to: " + (active ? "active" : "inactive"));
  let fill = active ? "" : "-inactive";
  browser.browserAction.setIcon({
    path: {
      "20": "images/eus-20" + fill + ".png",
      "40": "images/eus-40" + fill + ".png"
    }
  });
}

function browserActionSetup() {
  browser.browserAction.getPopup({}, (popupUrl) => {
    if (window.loadedSettings.settingsAutoUrlShorten) {
      // Remove popup
      browser.browserAction.setPopup({ popup: "" });

      // Hook extension icon click
      browser.browserAction.onClicked.addListener((tab) => {
        if (window.loadedSettings.settingsAutoUrlShorten) {
          processShortenUserRequest(tab.url);
        }
      });
    } else if (!popupUrl || popupUrl == "") {
      setup();
    }
  })
}

function handleSettingsChange(sett) {
  let defaultSettings = {
    settingsShowNotification: true,
    settingsShowCurrentUrlShorten: true,
    settingsAutoUrlShorten: false
  };

  if (sett) {
    if (!arraysEqual(Object.keys(sett), Object.keys(defaultSettings))) {
      console.log("Migrating old settings");
      let migratedSettings = defaultSettings;

      Object.keys(sett).forEach((key) => {
        if (defaultSettings[key] != undefined) {
          migratedSettings[key] = sett[key];
        }
      });

      browser.storage.local.set({
        settings: migratedSettings
      });
    } else {
      console.log("Loaded stored settings");
      window.loadedSettings = sett;
      console.log(window.loadedSettings);
    }
  }
  else {
    // If no settings exist, load default and save them
    console.log("Loaded default settings");
    browser.storage.local.set({
      settings: defaultSettings
    });
  }

  // Context item create
  createContextMenuItems();
  browserActionSetup();
}

function processShortenUserRequest(linkToShorten) {
  console.log("To be shortened:" + linkToShorten);
  if (window.loggedin) {
    shortenUrl(linkToShorten, window.accesstoken).then(clipboardWriteHandler);
  } else {
    shortenUrlAnonymous(linkToShorten).then(clipboardWriteHandler);
  }
}

function clipboardWriteHandler(dat) {
  let shortlink = "none";

  document.oncopy = function (event) {
    event.clipboardData.setData("Text", shortlink);
    event.preventDefault();
    console.log("Link copied to clipboard");
    console.log(shortlink);

    setTimeout(() => browser.runtime.sendMessage({ linkShortened: true }), 1500);


    if (window.loadedSettings.settingsShowNotification) {
      // Link generated notification
      browser.notifications.create("link-gen-notif-" + Date.now(), {
        "type": "basic",
        "requireInteraction": true,
        "iconUrl": browser.extension.getURL("images/eus-128.png"),
        "title": browser.i18n.getMessage("shortenNotificationTitle"),
        "message": browser.i18n.getMessage("shortenNotificationDescription")
      }, (nId) => {
        setTimeout(() => {
          browser.notifications.clear(nId, (res) => { console.log(res) });
        }, 6500);
      });
    }

    document.oncopy = null;
  };

  shortlink = dat;
  document.execCommand("copy", true, null)
}

async function shortenUrl(url, apikey) {
  let shortenReqData = JSON.stringify({ "long_url": url, "group_guid": window.defaultgroup.guid });
  let shortlink = await makeRequest("POST", "https://api-ssl.bitly.com/v4/shorten", apikey, shortenReqData);

  return shortlink.link;
}

async function shortenUrlAnonymous(url) {
  return makeRequestRaw("POST", "http://tinyurl.com/api-create.php?url=" + url, "", null);
}

function arraysEqual(arr1u, arr2u) {
  let arr1 = arr1u.sort();
  let arr2 = arr2u.sort();

  if (arr1.length !== arr2.length)
    return false;
  for (var i = arr1.length; i--;) {
    if (arr1[i] !== arr2[i])
      return false;
  }

  return true;
}