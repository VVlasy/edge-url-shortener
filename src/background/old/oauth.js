import { makeRequest, makeFormRequest } from './requests';

export function openOauthWebFlowLegacy(callback = () => { }) {
  window.clientid = "1f8e2650bf7418458f8ed5c7296cc1453f8f81f4";
  window.redirecturi = "http://urlshortener.vvlasy.cz/login.php";

  let creating = browser.tabs.create({
    url: "https://bitly.com/oauth/authorize?client_id=" + window.clientid +
      "&redirect_uri=" + window.redirecturi
  }, (tab) => {
    console.log("Opened oauth login");

    console.log("Added listener on oauth tab");
    var lstnr;
    browser.tabs.onUpdated.addListener(lstnr = (tabId, changeInfo, tabInfo) => {
      console.log("oAuth tab changed");
      console.log(changeInfo);
      let pattern = /^(http:\/\/urlshortener\.vvlasy\.cz\/success\.php\?accesstoken=)(.+)/;

      if (tabId == tab.id && changeInfo.url && pattern.test(changeInfo.url)) {
        setTimeout(() => { browser.tabs.remove(tab.id) }, 4000);

        console.log("User finished oAuth 2 web flow");

        console.log("Removed listener on oauth tab");
        browser.tabs.onUpdated.removeListener(lstnr);

        let urldata = pattern.exec(changeInfo.url);

        console.log("Retrieved access token");
        let token = urldata[2];
        console.log(token);

        makeRequest("GET", "https://api-ssl.bitly.com/v4/groups", token).then((groupsresult) => {
          console.log("Retrieved groups");
          console.log(groupsresult);
          // TODO: Get group corresponding to username
          makeRequest("GET", "https://api-ssl.bitly.com/v4/user", token).then((userresult) => {
            browser.storage.local.set({
              accesstoken: token,
              groups: groupsresult.groups,
              defaultgroup: groupsresult.groups[0],
              user: userresult
            });

            console.log("Retrieved user info")
            console.log(userresult);
            callback();
          });
        });
      }
    });
  });
}

export function openOauthWebFlow(callback = () => { }) {
  window.clientid = "1f8e2650bf7418458f8ed5c7296cc1453f8f81f4";
  window.secret = "6ac3e52ea797adab318da981434ef8cb88db7c5f";

  console.log(browser.identity.getRedirectURL("oauth2"));

  let authUrl = 'https://bitly.com/oauth/authorize?client_id=' +
    window.clientid +
    '&redirect_uri=' +
    browser.identity.getRedirectURL("oauth2");

  browser.identity.launchWebAuthFlow(
    {
      'url': authUrl,
      'interactive': true
    }, function (redirectUrl) {

      if(chrome.runtime.lastError){
        console.log('Login cancelled');
      }

      if (redirectUrl) {
        let token = redirectUrl.substr(chrome.identity.getRedirectURL("oauth2").length + 6);
        console.log('launchWebAuthFlow login successful:', token);
        console.log('Background login complete');

        /*let loginData = new FormData();
        loginData.append('client_id', window.clientid);
        loginData.append('client_secret', window.secret);
        loginData.append('code', token);
        loginData.append('redirect_uri', chrome.identity.getRedirectURL("oauth2"));*/

        let loginData = 'client_id=' + window.clientid + '&' +
          'client_secret=' + window.secret + '&' +
          'code=' + token + '&' +
          'redirect_uri=' + chrome.identity.getRedirectURL("oauth2");

        makeFormRequest("POST", "https://api-ssl.bitly.com/oauth/access_token", loginData).then((apiKey) => {
          let apiKeyData = apiKey.split('&');
          let key = apiKeyData[0].split('=')[1];
          console.log(key);

          makeRequest("GET", "https://api-ssl.bitly.com/v4/groups", key).then((groupsresult) => {
            console.log("Retrieved groups");
            console.log(groupsresult);
            // TODO: Get group corresponding to username
            makeRequest("GET", "https://api-ssl.bitly.com/v4/user", key).then((userresult) => {
              browser.storage.local.set({
                accesstoken: key,
                groups: groupsresult.groups,
                defaultgroup: groupsresult.groups[0],
                user: userresult,
                state: 'loggedIn'
              });
              browser.runtime.sendMessage({ stateChanged: true });

              console.log("Retrieved user info")
              console.log(userresult);
              callback();
            });
          });
        });
      }
    });
}

export function loadStoredData(callback) {
  browser.storage.local.get(["accesstoken", "defaultgroup", "settings", "state"], callback);
}

export function loadStoredUser(callback) {
  browser.storage.local.get(["user"], callback);
}