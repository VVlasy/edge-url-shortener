import React from "react";
import ReactDOM from "react-dom";

// Old Imports, rewrite into components later, get UI working right first
import { start } from './old/background';

function ExtensionBackground() {
  if (typeof browser === 'undefined') {
    console.log("Browser is chrome, populating 'browser' property");
    window.browser = chrome;
  }else{
    window.browser = browser;
  }

  console.log('Calling old Extension code');
  start();
}

ExtensionBackground();