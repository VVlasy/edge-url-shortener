import React from 'react';

export const ExtensionApiContext = React.createContext({
    browser: null,
    settings: {
        settingsShowNotification: false,
        settingsShowCurrentUrlShorten: false,
        settingsAutoUrlShorten: false
    },
    state: 'loading',
    setNewState: () => { },
    setNewSettings: () => { }
});