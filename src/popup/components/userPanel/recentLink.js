import React from 'react';
import { ExtensionApiContext } from '../../contexts/extensionApiContext';

class RecentLink extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className='link'>
                <p className='text-truncate'>{this.props.title}</p>
                <a href='#' onClick={() => {
                    this.context.browser.tabs.create({
                        url: this.props.link
                    })
                }}>{this.props.link}</a>
            </div>
        );
    }
}

RecentLink.contextType = ExtensionApiContext;
export default RecentLink;