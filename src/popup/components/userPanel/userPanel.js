import React from 'react';

import { ExtensionApiContext } from './../../contexts/extensionApiContext';
import LoadingCircles from '../loadingCircles';
import RecentLink from './recentLink';

import { makeRequest } from './../../../background/old/requests';

class UserPanel extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            userLink: '',
            accesstoken: '',
            defaultgroup: '',
            links: null,
            userText: () => 'userPreTitle',
            logoutButtonText: () => 'logoutButtonTitle',
            recentlyShortenedText: () => 'recentlyShortenedTitle',
            doUpdate: false,
            lastLinkTime: (new Date()).getTime()
        };
    }

    componentDidMount() {
        this.setState({
            userText: () => this.context.browser.i18n.getMessage("userPreTitle"),
            logoutButtonText: () => this.context.browser.i18n.getMessage("logoutButtonTitle"),
            recentlyShortenedText: () => this.context.browser.i18n.getMessage("recentlyShortenedTitle")
        });

        this.context.browser.storage.local.get(['user', 'accesstoken', 'defaultgroup'], (items) => {
            this.setState({
                username: items.user.name,
                userLink: 'https://app.bitly.com/' + items.defaultgroup.guid + '/bitlinks',
                accesstoken: items.accesstoken,
                defaultgroup: items.defaultgroup.guid
            });

            this.getLinks();
        });
    }

    componentDidUpdate() {
        if (this.state.doUpdate) {
            if ((new Date()).getTime() - this.state.lastLinkTime > 50) {
                this.getLinks();
            }
        }
    }

    getLinks() {
        if(this.context.showSettings)
            return;

        this.setState({
            doUpdate: false,
            links: null
        });

        makeRequest("GET",
            'https://api-ssl.bitly.com/v4/groups/' + this.state.defaultgroup + '/bitlinks?size=5&page=1',
            this.state.accesstoken).then((result) => {
                this.setState({
                    links: result.links,
                    doUpdate: true,
                    lastLinkTime: (new Date()).getTime()
                });
            });
    }

    renderLinks() {
        if (this.state.links == null)
            return (
                <div id="login-container">
                    <LoadingCircles />
                </div>
            );
        else if (this.state.links.length == 0)
            return (
                <RecentLink link='' title={this.context.browser.i18n.getMessage("recentlyShortenedEmpty")} />
            );
        else {
            let displayedLinks = [];

            this.state.links.forEach(link => {
                displayedLinks.push(<RecentLink key={link.id} link={link.link} title={link.title} />);
            });

            return displayedLinks;
        }
    }

    render() {
        return (
            <div id='login-container'>
                <div id="login-status">
                    <div className="user">
                        <p>
                            <span id="user-title">{this.state.userText()} </span>
                            <a id="username" href="#" onClick={() => {
                                this.context.browser.tabs.create({
                                    url: this.state.userLink
                                })
                            }}>{this.state.username}</a>
                        </p>
                    </div>
                    <div className="logout">
                        <button id="logoutButton" className="btn-logout" onClick={() => {
                            this.context.browser.runtime.sendMessage({
                                action: "logout"
                            });
                        }}>{this.state.logoutButtonText()}</button>
                    </div>
                </div>
                <div className="edge-divider"></div>
                <div id="recent-links">
                    <p id="recShortndTitle">{this.state.recentlyShortenedText()}</p>
                    <div id="links-container" className="link-list">
                        {this.renderLinks()}
                    </div>
                </div>
            </div>
        );
    }
}

UserPanel.contextType = ExtensionApiContext;
export default UserPanel;