import React from 'react';

import { ExtensionApiContext } from '../contexts/extensionApiContext';

class Login extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            buttonText: () => 'popupLoginButtonTitle'
        };
    }

    componentDidMount() {
        this.setState({
            buttonText: () => this.context.browser.i18n.getMessage("popupLoginButtonTitle")
        });
    }

    render() {
        return (
            <div id="login-action">
                <button id="loginButton" className="btn-login" onClick={() => {
                    this.context.browser.runtime.sendMessage({
                        action: "oauthlogin"
                    });
                }}>
                    {this.state.buttonText()}
                </button>
            </div>
        );
    }
}

Login.contextType = ExtensionApiContext;
export default Login;