import React from 'react';
import { ExtensionApiContext } from '../contexts/extensionApiContext';

class Footer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            footerText: () => 'poweredByFooterText'
        }
    }

    componentDidMount() {
        this.setState({
            footerText: () => this.context.browser.i18n.getMessage("poweredByFooterText")
        });
    }

    render() {
        return (
            <div id="login-footer" onClick={()=>console.log(this.context.state)}>
                <p>
                    <span id="pwrdby-footer">{this.state.footerText()} </span>
                    <span>bit.ly & tinyurl.com</span>
                </p>
            </div>
        );
    }
}

Footer.contextType = ExtensionApiContext;
export default Footer;