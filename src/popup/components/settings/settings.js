import React from 'react';

import { ExtensionApiContext } from '../../contexts/extensionApiContext';
import SettingsEntry from './settingsEntry';

class Settings extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            settingsTitle: () => 'poweredByFooterText'
        }
    }

    componentDidMount() {
        this.setState({
            settingsTitle: () => this.context.browser.i18n.getMessage("settingsTitle")
        });
    }

    renderContents() {
        if (this.props.visible)
            return (
                <div id="settings-root" className="settings">
                    <div id="settings-title">
                        <h4 className="translate" id="settingsTitle">{this.state.settingsTitle()}</h4>
                    </div>

                    <div className="edge-divider"></div>

                    <div className="settings-section">
                        <div className="link-list">
                            <SettingsEntry id='settingsShowNotification' />

                            <SettingsEntry id='settingsShowCurrentUrlShorten' />

                            <SettingsEntry id='settingsAutoUrlShorten' />
                        </div>
                    </div>
                </div>)
                ;
    }

    render() {
        return (
            <div id="settings-container">
                {this.renderContents()}
            </div>
        );
    }
}

Settings.contextType = ExtensionApiContext;
export default Settings;