import React from 'react';

import { ExtensionApiContext } from '../../contexts/extensionApiContext';

class OpenSettingsButton extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            displayedImage: "../images/settings.svg"
        }
    }

    componentDidUpdate() {
        let imageToBeShowed = this.context.showSettings ? "../images/exit.svg" : "../images/settings.svg";

        if (this.state.displayedImage != imageToBeShowed) {
            this.setState({
                displayedImage: imageToBeShowed
            });
        }
    }

    render() {
        switch (this.context.state) {
            case 'loggedOut':
            case 'loggedIn':
                return (
                    <div id="title-settings" onClick={this.props.onClick}>
                        <img id="title-settings-image" src={this.state.displayedImage} alt="" width="20" />
                    </div>
                );
            default:
                return '';
        };
    }
}


OpenSettingsButton.contextType = ExtensionApiContext;
export default OpenSettingsButton;