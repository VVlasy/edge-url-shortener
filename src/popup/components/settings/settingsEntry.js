import React from 'react';
import { ExtensionApiContext } from './../../contexts/extensionApiContext';

class SettingsEntry extends React.Component {
    constructor(props) {
        super(props);

        this.title = this.props.id + 'Title';
        this.description = this.props.id + 'Desc';

        this.state = {
            titleText: () => this.title,
            descriptionText: () => this.description,
        }
    }

    componentDidMount() {
        this.setState({
            titleText: () => this.context.browser.i18n.getMessage(this.title),
            descriptionText: () => this.context.browser.i18n.getMessage(this.description),
        });

        console.log(this.context);
    }

    render() {
        return (
            <div className="setting">
                <div className="setting-text">
                    <h6 id={this.title}>{this.state.titleText()}</h6>
                    <p id={this.description}>{this.state.descriptionText()}</p>
                </div>

                <div className="setting-toggle">
                    <div className="toggle-component">
                        <label className="toggle">
                            <ExtensionApiContext.Consumer>
                                {({ settings, setNewSettings }) => (
                                    <input id="showNotif" type="checkbox"
                                        checked={this.context.settings[this.props.id]}
                                        onChange={() => {
                                            this.context.settings[this.props.id] = !this.context.settings[this.props.id];
                                            this.context.setNewSettings(this.context.settings);
                                        }} />
                                )}

                            </ExtensionApiContext.Consumer>
                            <div>app-notifition</div>
                        </label>
                    </div>
                </div>
            </div>
        );
    }
}

SettingsEntry.contextType = ExtensionApiContext;
export default SettingsEntry;