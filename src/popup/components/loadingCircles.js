import React from 'react';

export default class LoadingCircles extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className='loading-circles'>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        );
    }
}