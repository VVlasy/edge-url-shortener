import React from "react";
import ReactDOM from "react-dom";

import { ExtensionApiMock, fetchStrings } from './ExtensionApiMock';
import { ExtensionApiContext } from './contexts/extensionApiContext';

import Footer from './components/footer';
import LoadingCircles from './components/loadingCircles';
import Login from './components/login';
import OpenSettingsButton from "./components/settings/openSettingsButton";
import Settings from "./components/settings/settings";
import UserPanel from "./components/userPanel/userPanel";

class ExtensionPopup extends React.Component {
  constructor(props) {
    super(props);

    let extApi;

    if (typeof browser === 'undefined') {
      console.log('Browser is chrome, populating \'browser\' property');
      extApi = chrome;
    } else {
      extApi = browser;
    }

    if (typeof extApi.i18n === 'undefined') {
      // development - mock needed Extension API Calls
      console.log('Extension API missing, using mock instead');
      fetchStrings(() => {
        setTimeout(this.setState({ browser: extApi }), 500)
      });
      Object.assign(extApi, ExtensionApiMock);
    }

    extApi.runtime.onMessage.addListener(this.handleBackendMessage.bind(this));

    this.state = {
      browser: extApi,
      settings: {
        settingsShowNotification: false,
        settingsShowCurrentUrlShorten: false,
        settingsAutoUrlShorten: false
      },
      state: 'loading',
      setNewState: this.setPopupState.bind(this),
      setNewSettings: this.setSettings.bind(this),
      showSettings: false
    }
  }

  handleBackendMessage(request, sender, sendResponse) {
    console.log('Received message from backend:', request);

    if (request.stateChanged) {
      this.checkState();
    } else if (request.linkShortened) {
      this.forceUpdate();
    }
  }

  checkState() {
    this.state.browser.storage.local.get(['state'], (items) => {
      this.setPopupState(items.state);
    });
  }

  setPopupState(newState) {
    this.setState({ state: newState });
  }

  setSettings(newSettings) {
    this.state.browser.storage.local.set({
      settings: newSettings
    });

    this.state.browser.runtime.sendMessage({
      action: "reload-settings"
    });

    this.setState({ settings: newSettings });
  }

  renderContents() {
    switch (this.state.state) {
      case 'loggedOut':
        return <Login />;
      case 'loggedIn':
        return <UserPanel />;
      default:
        return <LoadingCircles />;
    }

  }

  renderSettings() {
    switch (this.state.state) {
      case 'loggedOut':
      case 'loggedIn':
        return (<div>
          <OpenSettingsButton
            onClick={() => this.setState({ showSettings: !this.state.showSettings })} />
          <Settings visible={this.state.showSettings} />
        </div>);
    }
  }

  componentDidMount() {
    this.checkState();

    this.state.browser.storage.local.get("settings", (items) => {
      this.setSettings(items.settings);

      this.forceUpdate();
    });
  }

  render() {
    return <ExtensionApiContext.Provider value={this.state}>
      <div id="login-container">
        <div id="login-title">
          <h4>Edge Url Shortener</h4>
          {this.renderSettings()}
        </div>
        <div className='edge-divider'></div>
        {this.renderContents()}
        <div className='edge-divider'></div>
        <Footer />
      </div>
    </ExtensionApiContext.Provider>;
  }
}

const rootElement = document.getElementById('app');
ReactDOM.render(<ExtensionPopup />, rootElement);