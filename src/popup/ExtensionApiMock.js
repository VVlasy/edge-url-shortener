let strings = {};

export function fetchStrings(callback) {
    fetch('/src/public/_locales/' + navigator.language + '/messages.json').then((data) => {
        data.json().then((json) => {
            strings = json;
            console.log('fetched');
            callback();
        });
    });
}

export const ExtensionApiMock = {
    i18n: {
        getMessage: (text) => {
            let str = strings[text];

            if (typeof str !== 'undefined')
                return strings[text].message;
            else
                return text;

        }
    },
    storage: {
        local: {
            get: (params, callback) => {
                callback({
                    state: 'loggedIn',//loggedOut',
                    settings: {
                        settingsShowNotification: false,
                        settingsShowCurrentUrlShorten: false,
                        settingsAutoUrlShorten: false
                    },
                    user: {
                        name: 'test user',
                    },
                    defaultgroup: {
                        guid: 'x'
                    },
                    accesstoken: 'x'
                });
            },
            set: (data) => { }
        }
    },
    runtime: {
        sendMessage: () => { },
        onMessage: {
            addListener: () => { }
        }
    }
}