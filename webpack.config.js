const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackTemplate = require('html-webpack-template');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require('path');

const config = env => {
  return {
    entry: {
      popup: './src/popup/index.js',
      background: './src/background/index.js'
    },
    devtool: env == 'production' ? false : 'source-map',
    output: {
      path: path.resolve(__dirname, './build'),
      filename: '[name].js',
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          loader: {
            loader: 'babel-loader',
            options: {
              envName: env
            }
          },
          exclude: /node_modules/,
        },
      ],
    },
    resolve: {
      extensions: ['.js', '.jsx'],
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: HtmlWebpackTemplate,
        appMountId: 'app',
        chunks: ['popup'],
        template: './src/popup.html',
        filename: './popup.html'
      }),
      new CopyWebpackPlugin([{
        from: './src/public'
      }]),
      new CleanWebpackPlugin()
    ],
  }
};

module.exports = config;