<?php
$authcode = $_GET["code"];
$clientid = "1f8e2650bf7418458f8ed5c7296cc1453f8f81f4";
$clientsecret = "6ac3e52ea797adab318da981434ef8cb88db7c5f";
$base_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
$actual_link = $base_link."/login.php";

$result = getAccessToken("https://api-ssl.bitly.com/oauth/access_token", $clientid, $clientsecret, $authcode, $actual_link);

header('Location: '.$base_link."/success.php?accesstoken=".$result->access_token);//."&login=".$result->login);

function getAccessToken($url, $cid, $cis, $code, $ruri){
    $data = array(
        'client_id' => $cid,
        'client_secret' => $cis,
        'code' => $code,
        'redirect_uri' => $ruri
    );

    $ch = curl_init();
    $curlConfig = array(
        CURLOPT_URL            => $url,
        CURLOPT_POST           => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POSTFIELDS     => http_build_query($data),
        CURLOPT_HTTPHEADER =>array(
            "Content-type: application/x-www-form-urlencoded",
            "Expect:"
        ),
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false
    );
    curl_setopt_array($ch, $curlConfig);
    $result = curl_exec($ch);
    
    if ($result === false) {
        echo "ERROR: " . curl_error($ch) ."\r\n". curl_errno($ch) . "\r\n";
    }
    curl_close($ch);
    parse_str($result, $data);
    // Cast it to an object
    $data = (object)$data;

    return $data;
 }

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Logging in</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h1>Logging in...</h1>
    <h2>This might take a few a seconds</h2>
</body>
</html>