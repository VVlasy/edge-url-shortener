<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Success</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500&amp;subset=cyrillic-ext,latin-ext" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="success.css" />

    <!-- AdSense auto ads -->
    <!-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-7495964508545620",
            enable_page_level_ads: true
        });
    </script> -->
</head>

<body>
    <div class="container">
        <div id="title">
            <h1>Edge url shortener</h1>
        </div>
        <div id="success-mark">
            <svg id="successAnimation" class="animated" xmlns="http://www.w3.org/2000/svg" width="250" height="250" viewBox="0 0 70 70">
                <path id="successAnimationResult" fill="#D8D8D8" d="M35,60 C21.1928813,60 10,48.8071187 10,35 C10,21.1928813 21.1928813,10 35,10 C48.8071187,10 60,21.1928813 60,35 C60,48.8071187 48.8071187,60 35,60 Z M23.6332378,33.2260427 L22.3667622,34.7739573 L34.1433655,44.40936 L47.776114,27.6305926 L46.223886,26.3694074 L33.8566345,41.59064 L23.6332378,33.2260427 Z"
                />
                <circle id="successAnimationCircle" cx="35" cy="35" r="24" stroke="#979797" stroke-width="2" stroke-linecap="round" fill="transparent"
                />
                <polyline id="successAnimationCheck" stroke="#979797" stroke-width="2" points="23 34 34 43 47 27" fill="transparent" />
            </svg>
        </div>
        <div id="success-msg">
            <h2>Successfully logged in</h2>
        </div>
        <div class="ad-wrapper">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:block; text-align:center;"
                 data-ad-layout="in-article"
                 data-ad-format="fluid"
                 data-ad-client="ca-pub-7495964508545620"
                 data-ad-slot="7203159600"></ins>
            <script>
                 (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>

        <div id="footer">
            <p>Powered by bit.ly</p>
        </div>
    </div>

    <script src="../js/jquery-3.3.1.min.js"></script>

    <script>
        setTimeout(() => {
            $('.circle-loader').toggleClass('logged-in');
            $('.checkmark').toggle();
        }, 2000);
    </script>
</body>

</html>